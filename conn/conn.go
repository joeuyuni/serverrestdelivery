package conn

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	_ "github.com/denisenkom/go-mssqldb"
)

var server = "192.168.1.12:1433"
var port = 1433
var user = "saphi"
var password = "Delphi2007"
var database = "ClubKetal"

type DataConnection struct {
	Server   string `json:"server"`
	Port     string `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Database string `json:"database"`
	PortApp  string `json:"portapp"`
}

var Db *sql.DB
var Data DataConnection
var loggerc *log.Logger

func ConnectionSQLSERV(logger *log.Logger) {

	var err error
	loggerc = logger

	//connString := fmt.Sprintf("sqlserver://%s:%s@%s?database=%s&encrypt=disable", user, password, server, database)
	connString := ReadDataConnection()
	if Db == nil {
		//mssql.new
		Db, err = sql.Open("sqlserver", connString)
		if err != nil {
			//log.Fatal("Error creating connection pool: " + err.Error())
			loggerc.SetPrefix("--ConnectionSQLSERV")
			loggerc.Println("--Error creating connection pool: " + err.Error())

		}
		//log.Printf("\nConnected!\n")
		fmt.Println("++Connected dataBase ! ->IP :" + Data.Server + Data.Port + " base: " + Data.Database)
		//logger.SetPrefix("++Connected! ->")
		loggerc.Println("++Connected dataBase ! ->IP :" + Data.Server + Data.Port + " base: " + Data.Database)

	} else {
		loggerc.Println("**created dataBase ! ->IP :" + Data.Server + Data.Port + " base: " + Data.Database)
	}

}

func PinDB() bool {
	conect := true
	ctx := context.Background()
	// Ping database to see if it's still alive.
	// Important for handling network issues and long queries.
	err := Db.PingContext(ctx)
	if err != nil {
		loggerc.Println("Error pinging : " + err.Error())
		//log.Fatal("Error pinging : " + err.Error())
		conect = false
	}
	return conect
}

func Version() {
	var result string
	ctx := context.Background()
	// Run query and scan for result
	erroe := Db.QueryRowContext(ctx, "SELECT @@version").Scan(&result)
	if erroe != nil {
		loggerc.Fatal("--Error pinging : " + erroe.Error())
		log.Fatal("Scan failed:", erroe.Error())

	}
	//fmt.Printf("%s\n", result)
	loggerc.Printf("%s\n", result)
}

func ReadDataConnection() string {

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		//log.Fatal(err)
		loggerc.Fatal("Error pinging : " + err.Error())
	}
	content, err := ioutil.ReadFile(dir + "\\db\\mainConnection.txt")
	if err != nil {
		//log.Fatal(err)
		loggerc.Fatal("Error  : " + err.Error())
	}

	text := string(content)
	//fmt.Println(text)
	json.Unmarshal([]byte(text), &Data)
	if Data.Port != "" {
		Data.Port = ":" + Data.Port
	}
	connString := fmt.Sprintf("sqlserver://%s:%s@%s%s?database=%s&encrypt=disable", Data.User, Data.Password, Data.Server, Data.Port, Data.Database)

	return connString
}
