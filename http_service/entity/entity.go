package entity

import "time"

type cabeceraDel struct {
	Id           int32     `json:"id,omitempty"`
	Nit          string    `json:"nit,omitempty"`
	Razon_social string    `json:"razon_social,omitempty"`
	Nombre       string    `json:"nombre,omitempty"`
	Direccion    string    `json:"direccion,omitempty"`
	Telefono     string    `json:"telefono,omitempty"`
	IdExterno    string    `json:"idExterno,omitempty"`
	FECHA        time.Time `json:"fecha,omitempty"`
	Hora         string    `json:"hora,omitempty"`
	FECHA_ATT    time.Time `json:"FECHA_ATT,omitempty"`
	Trans        float64   `json:"trans,omitempty"`
	Estado       string    `json:"estado,omitempty"`

	//Address   *Address `json:"address,omitempty"`D:\proyect\goproject\httpServerKetalPos\http_service\entity\entity.go
}

type cliente struct {
	Carnet             float64
	fechaActualizacion time.Time
}
