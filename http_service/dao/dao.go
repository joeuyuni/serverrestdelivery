package dao

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strconv"

	mssql "github.com/denisenkom/go-mssqldb"
)

var db *sql.DB
var loggerd *log.Logger

func SetConn(newDb *sql.DB) {
	db = newDb
}

func SetLog(logger *log.Logger) {
	loggerd = logger

}

func Version() {
	var result string
	ctx := context.Background()

	// Run query and scan for result
	erroe := db.QueryRowContext(ctx, "SELECT @@version").Scan(&result)
	if erroe != nil {
		loggerd.Println("Scan failed:", erroe.Error())
	}
	fmt.Printf("%s\n", result)

}

func GetClient() (listEst []Cliente, err error) {
	ctx := context.Background()
	qsql := `
			SELECT
				 [Carnet]
		 	,[fechaActualizacion]
			
			 FROM [ClubKetal].[dbo].[Tbl_AuxCliente]
	`
	row, err := db.QueryContext(ctx, qsql)
	if err != nil {
		return
	}
	for row.Next() {
		c := Cliente{}

		err = row.Scan(
			&c.Carnet,
			&c.FechaActualizacion,
		)
		listEst = append(listEst, c)
	}
	defer row.Close()
	//fmt.Println(listEst)

	return listEst, err
}

func GetDelivery(startDate string, endDate string, id string, suc string, _type string) (ListCab []TBLDLYPedidoCabecera, err error) {
	ctx := context.Background()
	var rs mssql.ReturnStatus
	qsql := `
	exec [dbo].[sp_getDeliveryReport]  
	 @id , @type,  @fechaIni, @fechafin, @sala
	`
	row, err := db.QueryContext(ctx, qsql,
		sql.Named("id", id),
		sql.Named("type", _type),
		sql.Named("fechaIni", startDate),
		sql.Named("fechafin", endDate),
		sql.Named("sala", suc),
		&rs,
	)
	//fmt.Println(qsql)
	if err != nil {
		loggerd.SetPrefix("dao.GetDelivery")
		loggerd.Println(qsql)
		loggerd.Println("error: ", err.Error())
		return
	}
	for row.Next() {

		c := TBLDLYPedidoCabecera{}

		err = row.Scan(
			&c.Id,
			&c.NIT,
			&c.RazonSocial,
			&c.Nombre,
			&c.Direccion,
			&c.Telefono,
			&c.IdExterno,
			&c.FECHA,
			&c.FECHA_ATT,
			&c.Hora,
			&c.Trans,
			&c.Estado,
			&c.Sala,
		)
		c.Total = 0
		Listdete, _ := GetDeliveryDetalle(startDate, endDate, c.Id, suc, "7")
		for i := 0; i < len(Listdete); i++ {
			item := Listdete[i]
			if item.Estado != "-1" {
				c.Total = c.Total + (item.Precio * item.Cantidad)
				//c.Total = c.Total + (item.Precio)
			}

		}
		c.Ldetalle = Listdete

		ListCab = append(ListCab, c)
	}
	defer row.Close()
	return ListCab, err
}

func GetDeliveryDetalle(startDate string, endDate string, id string, suc string, _type string) (ListDet []TBLDLYPedidoDetalle, err error) {
	ctx := context.Background()

	/**
	EXEC [dbo].[sp_getDeliveryReport2]
	*/
	var rs mssql.ReturnStatus
	//fmt.Println(startdate)
	qsql := `
	exec [dbo].[sp_getDeliveryReport]  
	 @id , @type,  @fechaIni, @fechafin, @sala
	`
	row, err := db.QueryContext(ctx, qsql,
		sql.Named("id", id),
		sql.Named("type", _type),
		sql.Named("fechaIni", startDate),
		sql.Named("fechafin", endDate),
		sql.Named("sala", suc),
		&rs,
	)
	//fmt.Println(qsql)
	if err != nil {
		//fmt.Println("getDeliver sp  " + err.Error())
		loggerd.SetPrefix("dao.GetDeliveryDetalle")
		loggerd.Println(qsql)
		loggerd.Println("error: ", err.Error())
		return
	}
	for row.Next() {

		c := TBLDLYPedidoDetalle{}

		err = row.Scan(
			&c.IDDTL,
			&c.ID,
			&c.Codinter,
			&c.Cantidad,
			&c.Precio,
			&c.Tipopro,
			&c.Estado,
			&c.Descrip,
		)
		if err != nil {
			loggerd.SetPrefix(" dao.GetDeliveryDetalle ")
			loggerd.Println("error: ", err.Error())

		}

		ListDet = append(ListDet, c)
	}

	//ListCab.Ldetalle=append(ListCabListCab.Ldetalle,  )
	defer row.Close()
	//fmt.Println("sasa")

	return ListDet, err
}

func InsertDelivery(deli TBLDLYPedidoCabecera) (idin string, err error) {
	ctx := context.Background()
	var uid string
	/**
	EXEC [dbo].[sp_getDeliveryReport2]

	*/
	//var rs mssql.ReturnStatus
	//fmt.Println(startdate)
	qsql := `
	
	exec [dbo].[sp_insDeliveryCab]  
	@p_nit ,
	@p_razonSoc ,
	@p_nombre ,
	@p_direccion, 
	@p_telefono ,
	@p_idExt ,
	@p_sala ,
	@p_aux 

	`
	//fmt.Println(qsql)
	row, err := db.QueryContext(ctx, qsql,
		sql.Named("p_nit", deli.NIT),
		sql.Named("p_razonSoc", deli.RazonSocial),
		sql.Named("p_nombre", deli.Nombre),
		sql.Named("p_direccion", deli.Direccion),
		sql.Named("p_telefono", deli.Telefono),
		sql.Named("p_idExt", deli.IdExterno),
		sql.Named("p_sala", deli.Sala),
		sql.Named("p_aux", deli.Sala),
	)
	//fmt.Println(qsql)
	if err != nil {
		loggerd.SetPrefix("dao.InsertDelivery ")
		loggerd.Println(qsql)
		loggerd.Println("error: ", err.Error())
		//fmt.Println("cabecera  sp " + err.Error())
		return
	}

	for row.Next() {

		err = row.Scan(

			&uid,
		)
		if err != nil {
			loggerd.SetPrefix("dao.InsertDelivery row ")
			loggerd.Println("error: ", err.Error())

		}

		//ListDet = append(ListDet, c)
	}
	detalle := deli.Ldetalle
	numberID, err := strconv.Atoi(uid)

	if numberID > 0 {
		for i := 0; i < len(detalle); i++ {
			item := detalle[i]
			item.ID = uid
			err = InsertDeliveryDetalle(item, deli.Sala)
			if err != nil {
				loggerd.SetPrefix("dao.InsertDelivery detalle ")
				//loggerd.Println(qsql)
				loggerd.Println("error: ", err.Error())
			}
		}
	}

	//ListCab.Ldetalle=append(ListCabListCab.Ldetalle,  )
	defer row.Close()
	//fmt.Println(uid)

	return uid, err
}

func InsertDeliveryDetalle(deta TBLDLYPedidoDetalle, sala string) (err error) {
	ctx := context.Background()
	var uid string

	qsql := `
	
	exec [dbo].[sp_insinsDeliveryDet]
	@p_id ,
	@p_codinter ,
	@p_cantidad ,
	@p_precio,
	@p_tipoPro,
	@p_descripcion,
	@p_sala 

	`
	//fmt.Println(qsql)
	row, err := db.QueryContext(ctx, qsql,
		sql.Named("p_id", deta.ID),
		sql.Named("p_codinter", deta.Codinter),
		sql.Named("p_cantidad", deta.Cantidad),
		sql.Named("p_precio", deta.Precio),
		sql.Named("p_tipoPro", deta.Tipopro),
		sql.Named("p_descripcion", deta.Descrip),
		sql.Named("p_sala", sala),
	)
	//fmt.Println(qsql)
	if err != nil {
		//fmt.Println("detalle sp  " + err.Error())
		loggerd.SetPrefix("dao.InsertDeliveryDetalle ")
		loggerd.Println(qsql)
		loggerd.Println("error: ", err.Error())
		return
	}

	for row.Next() {

		err = row.Scan(
			&uid,
		)
		if err != nil {
			//fmt.Println("detalle row  " + err.Error())
			loggerd.SetPrefix("dao.InsertDeliveryDetalle ROW ")
			//loggerd.Println(qsql)
			loggerd.Println("error: ", err.Error())
		}

		//ListDet = append(ListDet, c)
	}

	//ListCab.Ldetalle=append(ListCabListCab.Ldetalle,  )
	defer row.Close()
	//fmt.Println("detalle " + uid)

	return err
}

func PinDB() bool {
	conect := true
	ctx := context.Background()
	// Ping database to see if it's still alive.
	// Important for handling network issues and long queries.
	stat := db.Stats()
	loggerd.Println(fmt.Sprintf("CONEXIONES abiertas = %d", stat.OpenConnections))
	loggerd.Println(fmt.Sprintf("CONEXIONES cerradas = %d", stat.MaxIdleClosed))
	loggerd.Println(fmt.Sprintf("numero de esperas = %d", stat.WaitCount))
	loggerd.Println(fmt.Sprintf("duracion = %d", stat.WaitDuration))
	//if stat.OpenConnections < 1 {
	//loggerd.Println("activando :")
	err := db.PingContext(ctx)
	if err != nil {
		loggerd.Println("Error pinging : " + err.Error())
		//log.Println("Error pinging : " + err.Error())
		conect = false
	}
	//}
	return conect
}
