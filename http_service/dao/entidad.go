package dao

import (
	"time"
)

type Cabe struct {
	Id           string `json:"id,omitempty"`
	Razon_social string `json:"razon_social,omitempty"`
	Direccion    string `json:"direccion,omitempty"`

	//Address   *Address `json:"address,omitempty"`D:\proyect\goproject\httpServerKetalPos\http_service\entity\entity.go
}

type Cliente struct {
	Carnet             uint      `json:"id,omitempty"`
	FechaActualizacion time.Time `json:"FechaActualizacion,omitempty"`
}

type TBLDLYPedidoCabecera struct {
	Id          string                `json:"id,omitempty"`
	NIT         string                `json:"nit"`
	RazonSocial string                `json:"razon_social"`
	Nombre      string                `json:"nombre"`
	Direccion   string                `json:"direccion"`
	Telefono    string                `json:"telefono"`
	IdExterno   string                `json:"idExterno"`
	FECHA       time.Time             `json:"fecha,omitempty"`
	FECHA_ATT   time.Time             `json:"fecha_att,omitempty"`
	Hora        string                `json:"hora,omitempty"`
	Trans       string                `json:"trans,omitempty"`
	Estado      uint                  `json:"estado"`
	Sala        string                `json:"sala"`
	Total       float64               `json:"total"`
	Ldetalle    []TBLDLYPedidoDetalle `json:"ldetalle"`

	//IDDTL    uint                  `json:"id_dtl"`
	//Codinter string                `json:"codinter,omitempty"`
	//Cantidad float64               `json:"cantidad"`

}

// TBLDLYPedidoDetalle --
type TBLDLYPedidoDetalle struct {
	IDDTL    string  `json:"id_dtl"`
	ID       string  `json:"id"`
	Codinter string  `json:"codinter"`
	Cantidad float64 `json:"cantidad,string"`
	Precio   float64 `json:"precio,string"`
	Tipopro  string  `json:"tipopro,omitempty"`
	Estado   string  `json:"estado,omitempty"`
	Descrip  string  `json:"descrip,omitempty"`
}

type Person struct {
	ID        string   `json:"id,omitempty"`
	FirstName string   `json:"firstname,omitempty"`
	LastName  string   `json:"lastname,omitempty"`
	Address   *Address `json:"address,omitempty"`
}

type Address struct {
	City  string `json:"city,omitempty"`
	State string `json:"state,omitempty"`
	//Cantidad float64 `json:"cantidad,string"`
}

//var Cabecera *cabeceraDel
