package http_service

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	str "strings"
	"time"

	"./dao"
	"github.com/gorilla/mux"
	//"./entity"
)

var db2 *sql.DB
var loggerw *log.Logger
var file *os.File

func GetIncomingDelivery(w http.ResponseWriter, req *http.Request) {
	dao.SetConn(db2)
	dao.SetLog(loggerw)

	keys := req.URL.Query()
	startDate := keys.Get("startDate") //Get return empty string if key not found
	endDate := keys.Get("endDate")     //Get return empty string if key not found
	suc := keys.Get("suc")             //Get return empty string if key not found
	id := keys.Get("id")               //Get return empty string if key not found

	if startDate == "" || endDate == "" {
		w.Write([]byte("[{\"warning\": \"" + "'startDate' y 'endDate' datos son necesarios  " + strconv.Itoa(http.StatusNoContent) + "\" }]"))
		return
	}
	dao.PinDB()
	ldelivery, err := dao.GetDelivery(startDate, endDate, id, suc, "6")
	if err != nil {
		w.Write([]byte("[{\"error\": \"" + "error base de datos " + strconv.Itoa(http.StatusConflict) + "\" }]"))
		loggerw.Println("[{\"error\": \"" + err.Error() + " " + suc + " " + strconv.Itoa(http.StatusNoContent) + "\" }]")
		//fmt.Println("Scan failed:", err.Error())
	} else {
		if ldelivery == nil {
			w.Write([]byte("[{\"warning\": \"" + "sin resultados para las busqueda  " + strconv.Itoa(http.StatusNoContent) + "\" }]"))
			loggerw.Println("[{\"warning\": \"" + "sin resultados para las busqueda  " + strconv.Itoa(http.StatusNoContent) + "\" }]")
		} else {
			json.NewEncoder(w).Encode(ldelivery)
		}

	}

}

func IncomingDelivery(w http.ResponseWriter, req *http.Request) {
	dao.SetConn(db2)
	dao.SetLog(loggerw)

	var deliveryIn dao.TBLDLYPedidoCabecera
	_ = json.NewDecoder(req.Body).Decode(&deliveryIn)

	//json.NewEncoder(w).Encode(deliveryIn)
	if deliveryIn.IdExterno == "" {
		deliveryIn.IdExterno = deliveryIn.NIT
		//w.Write([]byte("[{\"warning\": \"" + "'IdExterno' es ncesario !! " + strconv.Itoa(http.StatusNoContent) + "\" }]"))
		loggerw.Println("[{\"warning\": \"" + "'IdExterno' es CAMBIADO POR EL NIT !! " + strconv.Itoa(http.StatusNoContent) + "\" }]")

	}
	dao.PinDB()
	id, err := dao.InsertDelivery(deliveryIn)
	if err != nil {
		w.Write([]byte("[{\"error\": \"" + err.Error() + " " + deliveryIn.Sala + " " + strconv.Itoa(http.StatusNoContent) + "\"}]"))
		//fmt.Println("web insert del :", err.Error())
		loggerw.Println("[{\"error\": \"" + err.Error() + " " + deliveryIn.Sala + " " + strconv.Itoa(http.StatusNoContent) + "\"}]")

	} else {
		t := time.Now()
		now := t.Format("2006-01-02")
		fmt.Println("++host de envio -> " + req.URL.Host + " la fecha " + now + " sala " + deliveryIn.Sala + " id " + id)
		loggerw.Println("++host de envio -> " + req.URL.Host + " la fecha: " + now + " sala: " + deliveryIn.Sala + " id: " + id)
		ldelivery, errg := dao.GetDelivery(now, now, id, deliveryIn.Sala, "6")
		if errg != nil {
			w.Write([]byte("[{\"error\": \"" + err.Error() + strconv.Itoa(http.StatusNoContent) + "\"}]"))
			loggerw.Println("[{\"error\": \"" + err.Error() + strconv.Itoa(http.StatusNoContent) + "\"}]")
			//fmt.Println("web get del :", err.Error())
		}
		json.NewEncoder(w).Encode(ldelivery)

	}

}

func Listen(Dbb *sql.DB, port string, logger *log.Logger, oldf *os.File) {
	loggerw = logger
	db2 = Dbb
	file = oldf
	//Version()
	/*dao.SetConn(db2)
	dao.Version()
	dao.GetClient()*/

	router := mux.NewRouter()

	//s.HandleFunc("/people", GetIncomingDelivery).Methods("GET")
	//router.HandleFunc("/people", GetIncomingDelivery).Methods("GET")
	//router.HandleFunc("/people", GetIncomingDelivery).Methods("GET")
	//router.Handle("/api/demo/demo1", basicauthmiddleware.BasicAuthMiddleware(http.HandlerFunc(demoapi.Demo1API))).Methods("GET")
	//go router.Handle("/rest/delivery/{startDate}/{endDate}/{id}/{suc}", BasicAuthentication(http.HandlerFunc(GetIncomingDelivery))).Methods("GET")
	go router.Handle("/rest/delivery", BasicAuthentication(http.HandlerFunc(GetIncomingDelivery))).Methods("GET")
	go router.Handle("/rest/delivery", BasicAuthentication(http.HandlerFunc(IncomingDelivery))).Methods("POST")
	//router.ServeHTTP()
	portServer := ":8095"
	if port != "" {
		portServer = ":" + port
	}
	log.Println("servidor escuchado " + portServer)
	loggerw.Println("servidor escuchado " + portServer)
	loggerw.Fatalln(http.ListenAndServe(portServer, router))

	/*err := http.ListenAndServeTLS(":10443", "cert.pem", "key.pem", router)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}*/

}

func LogFileCreate() (*os.File, error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	t := time.Now()
	//now := t.Format("2006-01-02 15 04")
	now := t.Format("2006-01-02")
	var f *os.File
	//log.Println("manejando files " + file.Name())
	if file != nil {
		//log.Println(file.Name() + " not null----------------------------------------")
		if str.Contains(file.Name(), now) == false {
			//log.Println(" cerrado")

			file.Close()
			f, err = os.OpenFile(dir+"\\log\\"+now+"_Log.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
			if err != nil {
				log.Println(err)
			}
			file = f

		}
		f = file

	}

	return f, err

}

func loggerCreate(f *os.File) *log.Logger {

	/*if loggerw == nil {
		loggerw = log.New(f, " *inicio* ", log.LstdFlags)

		//logger.Println("nuevo")
	}*/

	logger := log.New(f, " *inicio* ", log.LstdFlags)
	//logger.Println("text to append")
	return logger
	//

}

func InitNewLog() *log.Logger {
	f, err := LogFileCreate()
	if err != nil {

		log.Println(err.Error())

	}

	logger := loggerCreate(f)
	return logger

}
