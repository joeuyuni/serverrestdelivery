package main

import (
	"log"
	"os"
	"path/filepath"
	"time"

	"./conn"
	serv "./http_service"
	//"github.com/gorilla/mux"
)

func main() {
	f, err := LogFileCreate()
	if err != nil {

		log.Println(err.Error())

	}
	logger := loggerCreate(f)

	logger.SetPrefix(" *main* \n")
	logger.Println("Ketal POS services se inicio con exito!!")

	conn.ConnectionSQLSERV(logger)
	conn.PinDB()
	conData := conn.Data
	serv.Listen(conn.Db, conData.PortApp, logger, f)
	 conn.Db.Close()
	defer f.Close()
}

func LogFileCreate() (*os.File, error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	t := time.Now()
	//now := t.Format("2006-01-02 15 04")
	now := t.Format("2006-01-02")
	var f *os.File
	if f != nil {
		defer f.Close()
	}
	f, err = os.OpenFile(dir+"\\log\\"+now+"_Log.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	return f, err

}

func loggerCreate(f *os.File) *log.Logger {
	logger := log.New(f, " *inicio* ", log.LstdFlags)
	//logger.Println("text to append")
	return logger
}
